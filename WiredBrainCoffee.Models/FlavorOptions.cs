﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WiredBrainCoffee.Models
{
    public enum FlavorOptions
    {
        None,
        Mocha,
        Vanilla,
        Caramel
    }
}
